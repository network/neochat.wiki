# After logging in to NeoChat, I can't read any existing decrypted mesages / How can I import decryption Keys / I verified my session in Element and still can't read encryted messages

As of 24.08, it is generally possible to load encryption keys for existing message, though the interface for it is somewhat hidden for now:
- Go to Settings -> General -> Developer Settings -> Activate "Enable developer tools"
- Directly below, open developer tools -> Feature Flags -> Activate "Secret Backup"
- Close the settings, right click on your avatar in the bottom-left corner of the window -> Open Secret Backup
- Enter your passphrase in the first box (this is not your account password. It is an additional secret that you may or may not have explicitely chosen when creating your account). If you don't have one...
- Enter your security key. This is a text consisting of random letters and numbers and was shown to you when registering your account in another client. You may have this printed or stored on disk.
- If your device is verified, you can try requesting the keys from another device by clicking "Request from other devices.

The dialog isn't great at giving feedback yet. If you see a banner showing just a number, something might have gone wrong - most likely the passphrase / security key you entered is wrong.

Give the dialog some time and then restart neochat. Your historical messages should now show up.

Don't create bug reports for any of this please - the entire cryptography backend in libquotient is currently being reworked entirely to be much more robust and all development effort is spent there. We thus won't fix any bugs with the current implementation.